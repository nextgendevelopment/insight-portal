# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for and where to get the repository? ###

* Quick summary
Trackunit API is used to fetch CAN data using GKD PWS and Trackunit Hardware using Trackunit API. Python was used as a language for creating communication
* Version
6.2.0-post
* [GITHUB]
(https://github.com/confluentinc/examples.git)


## Summary of set up - This step has allready been done as the part of this repository
* [STEP1]
PS C:\Users\nitish.gautam\Desktop\code\trackunit-api> git clone https://github.com/confluentinc/examples.git
* [STEP2]
PS C:\Users\nitish.gautam\Desktop\code\trackunit-api> cd .\examples\
* [STEP3]
PS C:\Users\nitish.gautam\Desktop\code\trackunit-api\examples> git checkout 6.2.0-post
Already on '6.2.0-post'
Your branch is up to date with 'origin/6.2.0-post'.

* Configuration
### Execute the below command once you update consumer_ccsr.py file with recommended change (allreday done as a part of this Github) to build image
* [STEP4] 
C:\GKD\insight-portal\src\examples\clients\cloud\python> docker run -v $HOME/.confluent/librdkafka.config:/root/.confluent/librdkafka.config -v C:\GKD\insight-portal\src\examples\clients\cloud\python:/work -it --rm cloud-demo-python bash

* [STEP5]
C:\GKD\insight-portal\src\examples\clients\cloud\python> docker build -t cloud-demo-python .

## How to run Docker image
* [STEP6]
C:\GKD\insight-portal\src\examples\clients\cloud\python> docker run -v $HOME/.confluent/librdkafka.config:/root/.confluent/librdkafka.config -v C:\GKD\insight-portal\src\examples\clients\cloud\python:/work -it --rm cloud-demo-python bash

* [STEP7 - Inside Docker Container] 
root@6c58ee660bcb:/# python ./consumer_ccsr.py -f $HOME/.confluent/librdkafka.config -t gkd-technologies-export-aemp-20-external